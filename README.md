# AI assignment 5 

This is the fifth in for the course "IMT3103 Introduction to artificial intelligence", and is done in collaboration between Maarten and Nataniel. This assignment is made without the codebase provided, with help from this video explanation from  \href{https://www.youtube.com/watch?v=p-gpaIGRCQI}{Standford niversity}.

\section{The task}
The task for assignment 5 was to implement CSP solver based on backtracking search that will solve sudoku boards. Included for the source code is a menu in order to easily change between the example boards. 
The task also asks us to include the number of times the BACKTRACK function was called, and the number of times your BACKTRACK function returned failure, for each of the four boards shown above. It also asks for a brief comment on each of these numbers.
