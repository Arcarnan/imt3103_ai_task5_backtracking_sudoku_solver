#!/usr/bin/python3


#== Sets up clear console command ==#
from os import system, name

def clear():
    # If windows
    if name == 'nt':
        _ = system('cls')
    # for mac/linux
    else:
        _ = system('clear')

if name == 'nt':
    # Activates the use of color in the terminal for windows
    import ctypes

    kernel32 = ctypes.windll.kernel32
    kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)

#=====================================================#
from enum import Enum, unique
import sys
import math
import time

# set recursion limit, needed for visual studio
sys.setrecursionlimit(100000)

#global parameters
row = 0
col = 0
backtrackCalls = 0
backtrackFailureReturns = 0

# Declaring colors
@unique
class Color(Enum):
    Clear = "\033[0m"
    Start = "\033[48;5;1m"
    Stop = "\033[48;5;4m"
    Forest = "\033[48;5;34m"
    Grass = "\033[48;5;82m"
    Mountain = "\033[48;5;240m"
    Water = "\033[48;5;33m"
    Road = "\033[48;5;94m"
    Path = "\033[48;5;93m"
    TextRed = "\033[38;5;9m"
    TextBlue = "\033[38;5;12m"
#== End Color ==#

# printing the sudoku board
class Sudoku:
    def __init__(self, file):
        self.tiles = list()
        self.sizex = 9
        self.sizey = 9

        y = 0
        for line in file:
            self.tiles.append([])

            # read all characters from file, and cast to int
            for c in line:
                if (c == "\n"):
                    break
                self.tiles[y].append(int(c))

            y += 1

    # print the board
    def print(self):
        print("")
        for x in range(0, 9):
            for y in range(0, 9):
                sys.stdout.write(str(self.tiles[x][y]))
                if y == 2 or y == 5:
                    sys.stdout.write(Color.Start.value + "|" + Color.Clear.value)

            print("")
            if x == 2 or x == 5:
                sys.stdout.write(Color.Start.value + "===+===+===\n" +  Color.Clear.value)
        print("")

# find the next unassigned location, the location with value of 0
def FindUnassignedLocation(sudoku):
    global row
    global col
    for x in range(row, 9):
        for y in range(col, 9):

            # if unassigned
            if sudoku.tiles[x][y] == 0:
                return True
            if col == 8:
                break
            col += 1

        if row == 8:
            break

        col = 0
        row += 1

    # if no more unassigned locations
    return False

def NoConflicts(sudoku, row, col, num):

    # check for conflicts in rows and columns
    for x in range(9):
        if sudoku.tiles[x][col] == num:
            return False

    for y in range(9):
        if sudoku.tiles[row][y] == num:
            return False

    # check for conflicts in 3 X 3 blocks of the sudoku board
    for x in range(3):
        for y in range(3):
            if sudoku.tiles[math.floor(row / 3) * 3 + x][math.floor(col / 3) * 3 + y] == num:
                return False

    return True

def Solve(sudoku):
    global row
    global col

    global backtrackCalls
    global backtrackFailureReturns
    backtrackCalls += 1

    # while there are unassigned locations
    if not FindUnassignedLocation(sudoku):
        return True
    myrow = row
    mycol = col

    # test a number to see if it can be placed here
    for x in range(1, 10):
        if (NoConflicts(sudoku, myrow, mycol, x)):
            sudoku.tiles[myrow][mycol] = x

            #when temporary valid number has been placed
            if (Solve(sudoku)):
                return True

        sudoku.tiles[myrow][mycol] = 0
        row = myrow
        col = mycol
    backtrackFailureReturns += 1
    return False

# menu for the sudoku solver, made for terminal
def Menu():
    print("Sudoku solver")
    print("Main menu")
    print("1 - easy.txt")
    print("2 - medium.txt")
    print("3 - hard.txt")
    print("4 - veryhard.txt")
    print("Q - Quit\n")
    return input("Choice: ")

running = True

while running:
    userinput = Menu()
    clear()
    if userinput == '1':
        sudoku = Sudoku(open("easy.txt"))
        Solve(sudoku)
        print("\nNumber of calls to backtrack function: ", backtrackCalls)
        print("\nNumber of failures returned: ", backtrackFailureReturns)
        print("\nDifference (calls - failures): ", backtrackCalls - backtrackFailureReturns)
        sudoku.print()
    if userinput == '2':
        sudoku = Sudoku(open("medium.txt"))
        Solve(sudoku)
        print("\nNumber of calls to backtrack function: ", backtrackCalls)
        print("\nNumber of failures returned: ", backtrackFailureReturns)
        print("\nDifference (calls - failures): ", backtrackCalls - backtrackFailureReturns)
        sudoku.print()
    if userinput == '3':
        sudoku = Sudoku(open("hard.txt"))
        Solve(sudoku)
        print("\nNumber of calls to backtrack function: ", backtrackCalls)
        print("\nNumber of failures returned: ", backtrackFailureReturns)
        print("\nDifference (calls - failures): ", backtrackCalls - backtrackFailureReturns)
        sudoku.print()
    if userinput == '4':
        sudoku = Sudoku(open("veryhard.txt"))
        Solve(sudoku)
        print("\nNumber of calls to backtrack function: ", backtrackCalls)
        print("\nNumber of failures returned: ", backtrackFailureReturns)
        print("\nDifference (calls - failures): ", backtrackCalls - backtrackFailureReturns)
        sudoku.print()
    if userinput == 'q' or userinput == 'Q':
        running = False

    # reset parameters between eack board being tested
    row = 0
    col = 0
    backtrackCalls = 0
    backtrackFailureReturns = 0
